#include <trackable.h>
#include <position.h>
#include <fstream>
#include <tools.h>

// Constructor(s)
template <typename T>
Trackable<T>::Trackable(std::string id) : _id(id) {}

// Public member method(s)
template <typename T>
void Trackable<T>::_addToLog(T n)
{
    _log.push_back(n);
}

template <typename T>
void Trackable<T>::printLogToFile()
{
    std::ofstream f;
    const std::string fileName = "visualize/" + _id + ".log";
    f.open(fileName);
    for (auto &i : _log)
    {
        f << tools::format(20, "", "", "") << i << "\n";
    }
    f.close();
}

/*!
This declatation is used just to make sure the compiler knows what
template types to expect. In this case we are using only Trackable<Position>
*/
Trackable<Position> trackablePostion("trackable");
