#include <angle.h>

bool Angle::isValid(double x, double y, double z)
{
  if (x < -180 || y < -180 || z < -180)
    return false;

  if (x > 180 || y > 180 || z > 180)
    return false;

  return true;
}

Angle::Angle(double x, double y, double z) : ThreeD(x, y, z)
{
  if (!this->isValid(x, y, z))
    throw std::invalid_argument("invalid arguments to create ThreeD instance");
}
