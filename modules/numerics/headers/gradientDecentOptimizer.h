#pragma once
#include <iostream>
#include <functional>
#include <types.h>
#include <abstractOptimizer.h>

class GradientDecentOptimizer : public AbstractOptimizer
{
private:
    double _accuracy;

public:
    GradientDecentOptimizer(double left, double right, double h, double learningRate, std::function<double(double)> &&function, double accuracy);

    /*!
    Starts the optimization process for the given function and accurcy (passed to constructor)
    Returns the maxima of the function
    @param left double
    @param right double
    @return double
    */
    double run();

private:
    /*!
    Approximates the derivative of _function.
    Please note the deriviation distance h is way bigger than it normally should be because _f is assumed to be a step function.
    So we choose h to be bigger than the function step width.
    @param x: double
    @return double
    */
    double _df(double);
};