#pragma once
#include <opticalMachine.h>
#include <types.h>
#include <parameterName.enum.h>

class ObjectiveFunction
{
private:
  double _totalBrightnessWeight;
  double _focusWeight;
  OpticalMachine *_om;
  std::string _componentName;
  ParameterName _parameterName;

public:
  ObjectiveFunction(OpticalMachine *);
  double computeTotalBrightness();
  double computeFocus();

  /*!
  Computed the value of the objective function using the current state of the opticalMachine.
  To make sure that the latest state of of the opticalMachine is being used, make sure to run the simulation of the opticalMachine
  by calling opticalMachine.run().
  */
  double compute();

  /*!
  Saves the compoennt- and parameter-name that you want to optimize.
  @param opticalComponentName string: the name of component to optimize
  @param parameter ParameterName: the name of the param to optimize (e.g. POSITIONX)
  */
  void setVariable(std::string opticalComponentName, ParameterName);

  /*!
  Acts like a mathematical function with one value input and one value output (R -> R)
  It should be used as a bridge between the optimizer which only understands mathematical functions and the photons simulator
  Every time this fucntion is called it will run change the parameters in the opticalMachine accordingly, run the simulation and
  finally evaluate the objective function.
  @param value: double
  @return double
  */
  double function(double);
  
  OpticalMachine *getOpticalMachine();
};