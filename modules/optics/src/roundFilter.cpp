#include <iostream>
#include <vector>
#include <roundFilter.h>

RoundFilter::RoundFilter(std::string name, Position p, Position n, double r, double min_wl, double max_wl) : RoundOpticalComponent(name, p, n, r), _minWaveLength(min_wl), _maxWaveLength(max_wl) {}

void RoundFilter::computePhotonDirection(Photon *photon)
{
    if (RoundOpticalComponent::isInside(photon))
    {
        // if wavelength of the photon is too low or too high
        if (photon->getWaveLength() < _minWaveLength || photon->getWaveLength() > _maxWaveLength)
        {
            // set direction of photon =0
            Position dir(0, 0, 0);
            photon->setDirection(dir);
        }
    }
    else
    {
        Position dir(0, 0, 0);
        photon->setDirection(dir);
    }
}
