#include <roundDetector.h>
#include <position.h>
#include <roundOpticalComponent.h>
#include <opticalComponent.h>
#include <types.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>

RoundDetector::RoundDetector(std::string name, Position p, Position n, double r) : RoundOpticalComponent(name, p, n, r) {}

void RoundDetector::computePhotonDirection(Photon *photon)
{
    Position new_dir(0, 0, 0);
    photon->setDirection(new_dir);
    photon->printLogToFile();
}
