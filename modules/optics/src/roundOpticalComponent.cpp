#include <roundOpticalComponent.h>
#include <opticalComponent.h>
#include <threeD.h>
#include <math.h>
#include <vector>
#include <fstream>
#include <tools.h>
#include <types.h>

RoundOpticalComponent::RoundOpticalComponent(std::string name, Position p, Position n, double r) : OpticalComponent(name, p, n), radius(r) {}

bool RoundOpticalComponent::isInside(Photon *photon)
{
    ThreeD distance = photon->getPosition() - this->getPosition();
    return distance.norm() <= radius;
}

void RoundOpticalComponent::saveComponentToFile()
{
    std::vector<ThreeD> borderPoints;
    const int nbOfPoints = 50;
    const double angularStep = 360 / nbOfPoints;

    for (double angle = 0; angle <= 360; angle += angularStep)
    {
        ThreeD point = this->getPlane().getPoint(radius, angle) + this->getPosition();
        borderPoints.push_back(point);
    }
    borderPoints.push_back(borderPoints[0]);

    std::ofstream f;
    std::string fileName = "visualize/" + this->getName() + ".dat";
    f.open(fileName);
    for (auto &point : borderPoints)
    {
        f << tools::format(30, "", " ", "") << point << "\n";
    }
    f.close();
}

Picture RoundOpticalComponent::getPicture(std::vector<Photon *> photons)
{
    Picture picture;
    double pic_size = 255;

    // std::reseize(...)
    for (int i = 0; i <= pic_size; i++)
    {
        picture.push_back(std::vector<int>());
        for (int j = 0; j <= pic_size; j++)
        {
            picture[i].push_back(0);
        };
    };

    // std::cout << "start get picture" << std::endl;
    for (std::vector<Photon *>::iterator p = std::begin(photons); p != std::end(photons); p++)
    {
        if (isInside(*p))
        {

            // Saving Photon to picture-matrix
            // Position diff = this->getPosition().operator-((*p)->getPosition()).convert<Position>();
            Position diff = ((*p)->getPosition() - this->getPosition()).convert<Position>();
            // std::cout << "diff: " << diff << ", photon:" << (*p)->getPosition() << ", component: " << this->getPosition() << ", radius: " << radius << std::endl;
            int n = 0;
            int m = 0;
            const int maxDiff = 2 * radius;

            // map position of photon to pixel of picture/ to entry of matrix
            if (std::round(diff.getX()) == 0)
            {
                // std::cout << "X" << std::endl;
                n = std::floor(((diff.getY() + radius) / maxDiff) * (pic_size - 1));
                m = std::floor(((diff.getZ() + radius) / maxDiff) * (pic_size - 1));
            }

            else if (std::round(diff.getY()) == 0)
            {
                // std::cout << "Y" << std::endl;
                n = std::floor(((diff.getX() + radius) / maxDiff) * (pic_size - 1));
                m = std::floor(((diff.getZ() + radius) / maxDiff) * (pic_size - 1));
            }

            else if (std::round(diff.getZ()) == 0)
            {
                // std::cout << "Z" << std::endl;
                n = std::floor(((diff.getX() + radius) / maxDiff) * (pic_size - 1));
                m = std::floor(((diff.getY() + radius) / maxDiff) * (pic_size - 1));
            };

            // for the brightness of each pixel we just count the number of photons hitting the same pixel
            picture.at(n).at(m) += 1;
            // picture[n][m] += 1;
            // std::cout << "Pixel at " << n << ',' << m << " : " << picture.at(n).at(m) << std::endl;
        };
    }

    // std::cout << "get picture done" << std::endl;
    return picture;
}

void RoundOpticalComponent::setParameter(ParameterName p, double value)
{
    if (p == POSITIONX)
    {
        Position newPositon = this->getPosition();
        newPositon.setX(value);
        this->setPosition(newPositon);
    }

    else if (p == POSITIONY)
    {
        Position newPositon = this->getPosition();
        newPositon.setY(value);
        this->setPosition(newPositon);
    }

    else if (p == POSITIONZ)
    {
        Position newPositon = this->getPosition();
        newPositon.setZ(value);
        this->setPosition(newPositon);
    }

    else if (p == RADIUS)
    {
        radius = value;
    }
}

double RoundOpticalComponent::getPropagationRadius()
{
    return radius;
}