#include <iostream>
#include <tinyxml.h>
#include <string>
#include <stdlib.h>
#include <vector>
#include <color.h>
#include <photon.h>
#include <opticalComponent.h>
#include <opticalMachine.h>
#include <roundFilter.h>
#include <roundLens.h>
#include <roundMirror.h>
#include <roundDetector.h>
#include <position.h>
#include <types.h>
#include <tools.h>
#include <fstream>
#include <algorithm>
#include <random>
#include <string>
#include <tools.h>

// Constructor(s)
OpticalMachine::OpticalMachine() {}

OpticalMachine::OpticalMachine(std::string XMLPath, std::string CSVPath, std::string resultPath, int maxPhotonsNb) : _XMLPath(XMLPath), _CSVPath(CSVPath), _resultPath(resultPath), _maxPhotonsNb(maxPhotonsNb)
{
    loadComponents(XMLPath);
    loadPhotons(CSVPath);
};

// Getter(s)
std::string OpticalMachine::getCSVPath() const
{
    return _CSVPath;
}

std::string OpticalMachine::getXMLPath() const
{
    return _XMLPath;
}

Picture OpticalMachine::getPicture()
{
    // setPicture(_components.back()->getPicture(_photons));
    return _picture;
}

OpticalComponent *OpticalMachine::getComponent(int index) const
{
    return _components[index];
}

OpticalComponent *OpticalMachine::getComponent(std::string name)
{
    for (auto &component : _components)
    {
        if (component->getName() == name)
        {
            return component;
        }
    }
    std::string errorMessage = "can't find component";
    throw std::invalid_argument(errorMessage);
}

Photon *OpticalMachine::getPhoton(int index) const
{
    return _photons[index];
}

// Setter(s)
void OpticalMachine::setCSVPath(std::string path)
{
    _CSVPath = path;
}

void OpticalMachine::setXMLPath(std::string path)
{
    _XMLPath = path;
}

void OpticalMachine::setPicture(Picture p)
{
    _picture = p;
}

void OpticalMachine::addComponent(OpticalComponent *oc)
{
    _components.push_back(oc);
}

void OpticalMachine::addPhoton(Photon *p)
{
    _photons.push_back(p);
}

// Public member methods
void OpticalMachine::run()
{
    for (auto &oc : _components)
    {
        oc->computePhotonsPosition(_photons);
        oc->computePhotonsDirection(_photons);
    };

    setPicture(_components.back()->getPicture(_photons));
}

void OpticalMachine::visualize()
{
    for (auto &oc : _components)
    {
        oc->saveComponentToFile();
    }

    system("cp visualize/gnuplotTemplate.p visualize/gnuplot.p");

    std::ofstream f;
    f.open("visualize/gnuplot.p", std::ios_base::app);
    f << "\n\n\n# Automatically generated script\n";
    f << "#photons plotting\n";
    f << "splot ";
    int i = 0;
    for (auto &photon : _photons)
    {
        f << "\"" << photon->getId() << ".log"
          << "\""
          << " u 1:2:3 with linespoints linestyle 1 notitle";
        f << ", \\\n";
    }

    for (auto &oc : _components)
    {
        f << "\"" << oc->getName() << ".dat\""
          << "u 1 : 2 : 3 lw 4 with lines title \"" << oc->getName() << "\"";
        f << ", \\\n";
    }

    f.close();
    system("cd visualize && gnuplot gnuplot.p");
}

void OpticalMachine::savePicture()
{
    // this->setPicture(_components.back()->getPicture(_photons));
    // Write to .img file
    int pic_size = _picture.size();
    std::ofstream img;
    img.open("./output.img");
    for (int i = 0; i < pic_size; i++)
    {
        for (int j = 0; j < pic_size; j++)
        {
            img << _picture[i][j];
            // std::cout << "image: " << _picture[i][j] << std::endl;
        };
        img << std::endl;
    };
    img.close();
}

std::string OpticalMachine::getXMLAttribute(TiXmlElement *oc, const char *attribute)
{
    try
    {
        return oc->Attribute(attribute);
    }
    catch (const std::exception &e)
    {
        std::string errorMessage = "can't read attribute '" + std::string(attribute) + "' of component '" + oc->Value() + "'\n";
        throw std::invalid_argument(errorMessage);
    }
}

OpticalComponent *OpticalMachine::getOpticalComponentFromXML(TiXmlElement *oc)
{
    using namespace tools;

    std::string componentName = oc->Value();
    bool isComponent = binary_search(this->allowedComponents.begin(), this->allowedComponents.end(), componentName);

    // define the expected attribute names that can be used in the XML file
    const char *NAME = "name";
    const char *RADIUS = "radius";
    const char *PX = "px", *PY = "py", *PZ = "pz";
    const char *NX = "nx", *NY = "ny", *NZ = "nz";
    const char *FOCAL_LENGTH = "focalLength";
    const char *MIN = "min", *MAX = "max";

    std::string type = oc->Value();
    std::string name = this->getXMLAttribute(oc, NAME);
    std::string radiusString = this->getXMLAttribute(oc, RADIUS);
    double radius = stringToDouble(radiusString);
    std::cout << "string raidus: " << radiusString << std::endl;
    Position position(
        stringToDouble(this->getXMLAttribute(oc, PX)),
        stringToDouble(this->getXMLAttribute(oc, PY)),
        stringToDouble(this->getXMLAttribute(oc, PZ)));

    Position normal(
        stringToDouble(this->getXMLAttribute(oc, NX)),
        stringToDouble(this->getXMLAttribute(oc, NY)),
        stringToDouble(this->getXMLAttribute(oc, NZ)));

    std::cout
        << "Component: " << name << std::endl
        << "\ttype: " << oc->Value() << std::endl
        << "\tradius: " << radius << std::endl
        << "\tposition: " << format(0, "{,}") << position << std::endl
        << "\tnormal: " << format(0, "{,}") << normal << std::endl;

    if (type == "Lens")
    {
        double focalLength = 0;
        focalLength = stringToDouble(this->getXMLAttribute(oc, FOCAL_LENGTH));
        std::cout << "\tfocal length: " << focalLength << std::endl;
        return new RoundLens(name, position, normal, radius, focalLength);
    }
    if (type == "Filter")
    {
        double min, max;
        min = stringToDouble(this->getXMLAttribute(oc, MIN));
        max = stringToDouble(this->getXMLAttribute(oc, MAX));
        std::cout << "\twave length bandwidth: [" << min << "," << max << "]" << std::endl;
        return new RoundFilter(name, position, normal, radius, min, max);
    }
    if (type == "Mirror")
    {
        return new RoundMirror(name, position, normal, radius);
    }
    if (type == "Detector")
    {
        return new RoundDetector(name, position, normal, radius);
    }
    std::cout << std::endl;
    return NULL;
}

// XML: 0-2position,3-5normal,6radius,7additional
void OpticalMachine::loadComponents(std::string fileName)
{
    std::vector<std::string> name;
    std::vector<std::vector<double>> parameter;

    // converting std::Strign to char[]
    char xmlFilename[fileName.length()];
    strcpy(xmlFilename, fileName.c_str());

    TiXmlDocument *pDocument = new TiXmlDocument();

    bool res = pDocument->LoadFile(xmlFilename);
    std::cout << "reading file " << fileName << "...\n";

    if (!res)
    {
        std::cout << "can't read xml file: " << fileName << "...\n";
    }

    /*
    Root is the container tag of all the  child components.
    It's called "OpticalMachine" in the xml file
    */
    TiXmlElement *root = pDocument->RootElement();

    std::cout << "xml output: " << root->Value() << "\n";

    for (TiXmlElement *el = root->FirstChildElement(); el != NULL; el = el->NextSiblingElement())
    {
        OpticalComponent *oc = this->getOpticalComponentFromXML(el);
        if (oc)
        {
            _components.push_back(oc);
        }
    }
}

void OpticalMachine::loadPhotons(std::string fileName)
{
    using namespace tools;

    std::ifstream f(fileName);
    std::string line;

    // std::cout << "reading file: " << fileName << "...\n";

    if (f.is_open())
    {
        int i = 0;
        int lineNb = 0;
        while (getline(f, line))
        {
            StringVector lineItems = split(line.substr(0, line.length() - 1), ",");
            // if case to ignore the first line in the csv file
            if (lineNb++ != 0)
            {
                std::string id = "photon_" + std::to_string(i++);
                Position photonPosition(stringToDouble(lineItems[1]), stringToDouble(lineItems[2]), stringToDouble(lineItems[3]));
                Photon *photon = new Photon(
                    photonPosition,
                    Position(1, 0, 0),
                    500,
                    id);
                addPhoton(photon);
            }
        }
        f.close();

        int initialPhotonsNb = _photons.size();
        int photonsPerPoint = _maxPhotonsNb / initialPhotonsNb;

        Photons initialPhotons = _photons;
        for (auto &p : initialPhotons)
        {
            generatePhotonsFromOnePoint(p->getId(), p->getPosition(), 500, photonsPerPoint);
        }
    }
    else
    {
        std::cout << "error wile reading file " << fileName << "\n";
    }
}

void OpticalMachine::generatePhotonsFromOnePoint(std::string id, Position point, double waveLength, int photonsPerPoint)
{
    if (this->_components.size() != 0)
    {
        const double radius = _components.front()->getPropagationRadius();
        Plane plane = _components[0]->getPlane();

        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_real_distribution<> radiusDistr(0, radius);
        std::uniform_real_distribution<> angleDistr(0, 360);

        for (int i = 0; i < photonsPerPoint; i++)
        {
            double randomRadius = radiusDistr(gen);
            double randomAngle = angleDistr(gen);
            Position randomPoint = (plane.getPoint(randomRadius, randomAngle) + _components[0]->getPosition()).convert<Position>();
            Position direction = (randomPoint - point).convert<Position>();
            std::string photonId = id + "_" + std::to_string(i);
            Photon *p = new Photon(point, direction, waveLength, photonId);
            addPhoton(p);
        }
    }
}

void OpticalMachine::setParameter(std::string name, ParameterName parameterName, double value)
{
    getComponent(name)->setParameter(parameterName, value);
}

void OpticalMachine::resetPhotons()
{
    _photons.clear();
    loadPhotons(_CSVPath);
}