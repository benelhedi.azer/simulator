#pragma once
#include <gradientDecentOptimizer.h>
#include <types.h>
#include <math.h>
#include <lineSearchOptimizer.h>

void optimizerTest()
{
    auto polynom = [](double x)
    {
        return -(x * x) + 2 * x;
    };

    auto discretePolynom = [](double x)
    {
        return round((-(x * x) + 2 * x) * 10) / 10;
    };

    const double accuracy = 0.01, h = 2, learningRate = 0.1;
    const double start = 0, end = 9;

    GradientDecentOptimizer gradientDecent(start, end, h, learningRate, discretePolynom, accuracy);
    double maximum = gradientDecent.run();
    std::cout << "[modified gradient decent] maximum at: " << maximum << std::endl;

    LineSearchOptimizer lineSearch(start, end, h, learningRate, discretePolynom);
    maximum = lineSearch.run();
    std::cout << "[line search] maximum at: " << maximum << std::endl;
}