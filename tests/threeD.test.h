#include <iostream>
#include <threeD.h>
#include <color.h>
#include <angle.h>
#include <position.h>

void threeDTest()
{

  std::cout << "ThreeD test\n";
  
  ThreeD a(5, 2, 3);
  std::cout << a << "\n";

  ThreeD b = a * 2;
  std::cout << b << "\n";

  Color c(15, 13, 100);
  std::cout << c << "\n";

  /*   Color d(15, 256, 100);
    std::cout << d << "\n"; */

  Angle e(50, 30, 80);
  std::cout << e << "\n";

  /*   Angle f(50, -181, 80);
    std::cout << f << "\n"; */

  Position p(20, 50, 80);
  std::cout << p << "\n";
}