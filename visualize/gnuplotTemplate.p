set terminal wxt size 1000,1000 enhanced font 'Verdana,10' persist
set view equal xyz

# Axes label
set xlabel 'x'
set ylabel 'y'
set zlabel 'z'

# Axes range
set xrange [0:15]
set yrange [0:15]
set zrange [0:15]

set autoscale x
set autoscale y
set autoscale z

# Grid
set grid xtics mxtics
set mxtics 3
set grid ytics mytics  
set mytics 3   
set grid ztics mztics
set mztics 3
set grid

# Remove ZAxis offset
set xyplane 0

# Define linestyle 1
set style line 1 \
    pointtype 7 pointsize 0.8

#set object circle at first 0,0 size scr 0.1 fc rgb "navy"

# Creating photons lines
#splot "photon_0.log" u 1:2:3 with linespoints linestyle 1 notitle, \
#      "photon_1.log" u 1:2:3 with linespoints linestyle 1 notitle

#splot "Mirror.dat" u 1:2:3 lw 4 with lines title "Mirror" , \
#    "Filter.dat" u 1:2:3 lw 4 with lines title "Filter"