# create an archive containing all library files.
# deploy the archive a website where it could be downloaded by anyone.
deploy:
	echo "deplying optiks library: not implemeted yet."

# run all unit and integration tests .
test: compile
	cd tests && make test

# runn all unit and integration tests and save console output to a log file
test-with-log: compile
	cd tests && make test-with-log

# generate a protable and ready-to-use library under build/
compile:
	cd modules && make build

# install all needed tools and packages for dev environement.
# note: this works only for ubuntu systems.
install-dev-dependencies:
	sudo apt-get install g++
	sudo apt install make
	sudo apt-get install -y gnuplot
	sudo apt-get install gnuplot-x11
	sudo apt-get install doxygen

#generate documentaion with doygen
doc:
	doxygen
	cd docs/html && firefox index.html

# remove test generated files and folders.
clean:
	cd tests && make clean
	rm -f visualize/*.dat visualize/*.log visualize/gnuplot.p  
	rm -rf docs/html
	clear

# remove build genetated files (.a library and .h headers).
clean-build:
	cd modules	&& make clean
	clear
