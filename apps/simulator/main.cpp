#include <iostream>
#include <opticalMachine.h>

int main()
{
    OpticalMachine om("components.xml", "photons.csv");

    om.run();

    //om.visualize();
    
    return 0;
}