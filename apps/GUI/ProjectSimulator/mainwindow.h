#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <opticalMachine.h>
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    QString path_csv;
    QString path_xml;
    QString path_directory;
    OpticalMachine *om;
    int number_photons;

private slots:
    void on_Browse_clicked();
    void on_Browse2_clicked();
    void on_SetDirectory_clicked();
    void on_SetValue_clicked();
    void on_Next_clicked();
    void on_Next2_clicked();
    void on_Next3_clicked();
    void on_Start_clicked();
    void on_Optimize_clicked();
    void on_Restart_clicked();
    void on_Test_clicked();
    void startSimulation();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
