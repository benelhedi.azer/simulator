#pragma once
#include <iostream>
#include <functional>
#include <types.h>

class Optimizer
{
private:
    double _h;
    double _left;
    double _right;
    double _learningRate;
    std::function<double(double)> _function;

    double _accuracy;

    /*
    @param left: double
    @param right: double
    @return bool

    checks whether the given optimization interval is valid to be used by the optimizer
    */
    void _checkInterval();

    /*
    @param x: double
    @return double

    approximates the derivative of _function
    */
    double _df(double);

public:
    /*
    @param function: std::function<double(double)> (supports lambda functions)
    @param accuracy: double
    */
    Optimizer(double h, double left, double right, double learningRate, std::function<double(double)> &&function, double accuracy);

    /*
    @param left: double
    @param right: double
    @return double

    starts the optimization process for the given function and accurcy (passed to constructor)
    and returns the optimized value (the maximum point)
    */
    double run();
};