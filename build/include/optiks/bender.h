#pragma once
#include <iostream>
#include <roundOpticalComponent.h>

class Bender : public RoundOpticalComponent
{
public:
  Bender(std::string name, Position p, Position n, double r);
  void computePhotonDirection(Photon *photon);
  void setParameter(ParameterName p, double value);
};
