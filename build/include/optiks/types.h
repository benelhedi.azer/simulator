#pragma once
#include <vector>
#include <photon.h>
#include <opticalComponent.h>
#include <color.h>
#include <functional>

typedef std::vector<Photon *> Photons;
typedef std::vector<OpticalComponent *> Components;
typedef std::vector<std::vector<int>> Picture;
typedef std::vector<std::string> StringVector;
typedef std::function<double(double)> OneOneFunction;