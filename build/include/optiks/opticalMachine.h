#pragma once
#include <types.h>
#include <tinyxml.h>

class OpticalMachine
{
private:
    std::string _CSVPath;
    std::string _XMLPath;
    std::string _resultPath;
    int _maxPhotonsNb;
    Picture _picture;
    Components _components;
    Photons _photons;
    StringVector allowedComponents = {"Detector", "Filter", "Lens", "Mirror"};

public:
    // Constructor(s)
    OpticalMachine();

    /*
    @param XMLPath: std::string
    @param CSVPath: std::string
    @param resultPath: std::string
    @param maxPhotonsNb: int
    */
    OpticalMachine(std::string XMLPath, std::string CSVPath, std::string resultPath, int maxPhotonsNb);

    // Getter(s)
    std::string getCSVPath() const;
    std::string getXMLPath() const;
    Picture getPicture();
    OpticalComponent *getComponent(int index) const;
    Photon *getPhoton(int index) const;
    OpticalComponent *getComponent(std::string name);

    // Setter(s)
    void setCSVPath(std::string);
    void setXMLPath(std::string);
    void setPicture(Picture);
    void addComponent(OpticalComponent *);
    void addPhoton(Photon *);

    // Public member method(s)

    /*!
    Loads component from XML file to OpticalMachine
    @param fileName std::string
    */
    void loadComponents(std::string fileName);

    /*!
    Loads photons from CSV file to OpticalMachine
    @param fileName std::string
    */
    void loadPhotons(std::string fileName);

    /*!
    Runs the simulation with the loaded photons and optical components
    */
    void run();

    /*!
    Take photons and put them into a picture.
    @param photons std::vector<Photon *>
    */
    void generatePicture(std::vector<Photon *>);
    
    /*!
    Saves the picture of the photons (if the picture is already token) into a .txt file 
    */
    void savePicture();

    /*!
    Visualizes the opticalMachine including photons and optical components using gnuplot.
    This function will sutomatically generate gnuplot script.
    */
    void visualize();

    /*!
    Sets one parameter of one component.
    @param componentName std::string
    @param OpticalComponentParameterName ParameterName
    @param value double
    */
    void setParameter(std::string name, ParameterName p, double value);
    
    /*!
    Saves opticalMachine configuration to an XML file.
    */
    void saveMachineToFile(std::string path);

    /*!
    Removes all photons from opticalMachine and load them again from the CSV file.
    */
    void resetPhotons();

private:
    // Private member methods
    OpticalComponent *getOpticalComponentFromXML(TiXmlElement *);
    std::string getXMLAttribute(TiXmlElement *oc, const char *attribute);
    void generatePhotonsFromOnePoint(std::string id, Position point, double waveLength, int photonsPerPoint);
};
